//
//  DataProvider.swift
//  ClockKit WatchKit Extension
//
//  Created by Jannis Rauschke on 20.05.19.
//  Copyright © 2019 Jannis Rauschke. All rights reserved.
//

import Foundation

// MARK: - Data structures for lectures

// Different types of lectures.
enum LectureType: String {
    
    case Statistics = "Statistics"
    case Swift = "Swift"
    case CompilerConstruction = "Compiler Cons."
    
}

// An item containing a date.
protocol Attendable {
    
    var time: Date { get }
    
}

// One lecture with a type and start time.
struct Lecture: Attendable {
    
    let type: LectureType
    let time: Date
    
}

// Extends arrays containing items conform to the Attendable protocol.
extension Collection where Iterator.Element: Attendable {
    
    // Returns the next lecture starting after the current time. This assumes that the given collection is sorted, since the first lecture matching the criteria is returned.
    func nextLecture() -> Iterator.Element? {
        let now = Date()
        return self.first {
            return $0.time > now
        }
    }
    
}

// MARK: - Data provider for the complications controller

// Data provider for an array of todays lectures.
struct DataProvider {
    
    func getTodaysLectures() -> [Lecture] {
        var lectures = [Lecture]()
        
        var now = Date()
        
        // advance the time by one minute intervals until it is dividable by 15
        while (Calendar.current.component(.minute, from: now) % 15 != 0){
            now.addTimeInterval(TimeInterval(60))
        }
        
        // add all lectures of today with an offset of 2 hours
        lectures.append(Lecture(type: .CompilerConstruction, time: now.addingTimeInterval(TimeInterval(-2 * 60 * 60))))
        lectures.append(Lecture(type: .Statistics, time: now))
        lectures.append(Lecture(type: .Swift, time: now.addingTimeInterval(TimeInterval(2 * 60 * 60))))
        
        return lectures
    }
    
}
