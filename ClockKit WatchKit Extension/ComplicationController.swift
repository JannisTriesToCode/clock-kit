//
//  ComplicationController.swift
//  ClockKit WatchKit Extension
//
//  Created by Jannis Rauschke on 11.04.19.
//  Copyright © 2019 Jannis Rauschke. All rights reserved.
//

import ClockKit

class ComplicationController: NSObject, CLKComplicationDataSource {
    
    let dataProvider = DataProvider()
    
    // Time of the day in percent.
    var dayFraction: Float {
        let now = Date()
        let calendar = Calendar.current
        let componentFlags = Set<Calendar.Component>([.year, .month, .day, .weekOfYear, .hour, .minute, .second, .weekday, .weekdayOrdinal])
        var components = calendar.dateComponents(componentFlags, from: now)
        components.hour = 0
        components.minute = 0
        components.second = 0
        let startOfDay = calendar.date(from: components)!
        return Float(now.timeIntervalSince(startOfDay)) / Float(24 * 60 * 60)
    }
    
    // MARK: - Timeline Configuration
    
    // This method is called to determine the time travel direction.
    func getSupportedTimeTravelDirections(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        // this enables time travel into the future
        handler([.forward])
    }
    
    // This method is called to get the time for the first item on the timeline (for time travel).
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        // returns the start time of the earliest lecture
        // assumes that the data provider returns a sorted collection
        handler(dataProvider.getTodaysLectures().first?.time)
    }
    
    // This method is called to get the time for the last item on the timeline (for time travel).
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        // returns the start time of the last lecture
        // assumes that the data provider returns a sorted collection
        handler(dataProvider.getTodaysLectures().last?.time)
    }
    
    // This method is called to determine whether complication data should be shown when the watch is locked.
    func getPrivacyBehavior(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        // show complication data even when the watch is locked
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    // This method is called to show a complication on the actual watch face.
    func getCurrentTimelineEntry(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        switch(complication.family) {
        case .graphicBezel:
            // simple static text
            let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: graphicBezelTemplate())
            handler(entry)
        case .graphicCorner:
            // shows the upcoming lecture
            if let lecture = dataProvider.getTodaysLectures().nextLecture() {
                let entry = graphicCornerTimelineEntry(lecture)
                handler(entry)
            } else {
                handler(nil)
            }
        case .graphicCircular:
            // shows the fraction of the day in a gauge
            let entry = CLKComplicationTimelineEntry(date: Date(), complicationTemplate: graphicCircularTemplate())
            handler(entry)
        default:
            // unsupported complication type
            handler(nil)
        }
    }
    
    // Returns a graphic corner timeline entry for the given lecture.
    func graphicCornerTimelineEntry(_ lecture: Lecture) -> CLKComplicationTimelineEntry {
        let template = graphicCornerTemplate(lecture)
        return CLKComplicationTimelineEntry(date: lecture.time, complicationTemplate: template)
    }
    
    // This method is called to get timeline entries before the given date (for time travel). The returned timeline entrys should be ordered ascending by time.
    func getTimelineEntries(for complication: CLKComplication, before date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // since we only support time travel into the future, we don't have to provide this method
        handler(nil)
    }
    
    // This method is called to get timeline entries after the given date (for time travel). The returned timeline entries should be ordered ascending by time.
    func getTimelineEntries(for complication: CLKComplication, after date: Date, limit: Int, withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        if (complication.family != .graphicCorner) {
            // we only support time travel for the graphic corner
            return handler(nil)
        }
        
        let entries = Array(dataProvider.getTodaysLectures().filter {
            // get all lectures starting after the given date
            return $0.time > date
        }.map {
            // generate timeline entries for the lectures
            return self.graphicCornerTimelineEntry($0)
        }.prefix(limit)) // only take up to limit lectures
        
        handler(entries)
    }
    
    // MARK: - Placeholder Templates
    
    // Returns the graphic corner complication template for the given lecture.
    func graphicCornerTemplate(_ lecture: Lecture) -> CLKComplicationTemplate {
        let template = CLKComplicationTemplateGraphicCornerStackText()
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "HH:mm"
        
        template.innerTextProvider = CLKSimpleTextProvider(text: lecture.type.rawValue)
        template.outerTextProvider = CLKSimpleTextProvider(text: "\(dateFormatter.string(from: lecture.time)) 🕒")
        
        return template
    }
    
    // Returns the graphic bezel complication template with the text "Zoltan's Watch".
    func graphicBezelTemplate() -> CLKComplicationTemplate {
        let template = CLKComplicationTemplateGraphicBezelCircularText()
        
        template.textProvider = CLKSimpleTextProvider(text: "Zoltan's Watch")
        template.circularTemplate = CLKComplicationTemplateGraphicCircular()
        
        return template
    }
    
    // Returns the graphic circular complication template with a gauge showing the fraction of the day.
    func graphicCircularTemplate() -> CLKComplicationTemplate {
        let template = CLKComplicationTemplateGraphicCircularClosedGaugeText()
        
        template.centerTextProvider = CLKSimpleTextProvider(text: "🕒")
        template.gaugeProvider = CLKSimpleGaugeProvider(style: CLKGaugeProviderStyle.ring, gaugeColor: UIColor.white, fillFraction: self.dayFraction)
        
        return template
    }

    // This method will be called once per supported complication, and the results will be cached. The returned complication template is shown to the user while configuring a watch face.
    func getLocalizableSampleTemplate(for complication: CLKComplication, withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        switch(complication.family) {
        case .graphicBezel:
            // simple static text
            handler(graphicBezelTemplate())
        case .graphicCorner:
            // shows the upcoming lecture
            if let lecture = dataProvider.getTodaysLectures().nextLecture() {
                handler(graphicCornerTemplate(lecture))
            } else {
                handler(nil)
            }
        case .graphicCircular:
            // shows the fraction of the day in a gauge
            handler(graphicCircularTemplate())
        default:
            // unsupported complication type
            handler(nil)
        }
    }
    
}
