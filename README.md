# ClockKit

This project demonstrates the features of the ClockKit complications.

## Members

- Felix Schütz
- Jannis Rauschke

## Instructions

1. Open this project in XCode
2. Change scheme (image 1)
2. Select "ClockKit WatchKit App (Complication)" and choose an iOS simulator with an iPhone and a paired Apple Watch (image 2)
4. Launch the project (Cmd + R)

<img src="readme_images/instruction_step_1.png">
<img src="readme_images/instruction_step_2.png">

To use the ClockKit complications provided by this project, do the following:

1. Force touch on the watch face
2. Scroll to the "Infograph" watch face (should be to the right)
3. Tap "Customize"
4. Scroll to the right to cusotmize the complications: [Photo Guide](#photo-guide)
5. Click on one of the complication types
6. Scroll until "ClockKit" is selected
7. Repeat for every complication type of the watch face
8. Click on the digital crown on the top right twice to end the customization and see the updated watch face

### Photo Guide

<img src="readme_images/complication_selection_1.png" width=25% height=25%>
<img src="readme_images/complication_selection_2.png" width=25% height=25%>
<img src="readme_images/complication_selection_3.png" width=25% height=25%>
&rarr;
<img src="readme_images/complication_selected.png" width=25% height=25%>

## Complications for the ClockKit

The ClockKit allows the displaying of variable-based (e.g. time-based) information in the clock view of the Apple Watch. This is done through complications. There are various types of complication templates regarding size and form. Depending on the complication type it can display text, a gauge or images, sometimes even at the same time. This information is passed as a timeline entry for a specific comlication type. Hence an event is set to a specific point in time and displayed on the clock.

The complication templates in this project are the following:

### Graphic Bezel

"Graphic Bezel" is an item in the "Infograph" watch face. I can either be a text or a gauge. In this project a simple static text is used.

<img src="readme_images/complication_selection_2.png" width=25% height=25%>

### Graphic Circular

"Graphic circular" is a circural field which can hold text, gauges or pictures. If wanted also a combination of them. As a simple demonstration of a gauge with text, the current fraction of the day is shown. Furthermore to indicate its relevance to the time, but also to save space the clock smiley is shown.

<img src="readme_images/complication_selection_1.png" width=25% height=25%>

### Graphic Corner

"Graphic Corner" is able to display text, gauges and pictures, and also combinations of them just as the other templates. Additionally it can display two lines of plain text. It is used to demonstrate the more advanced features of ClockKit. 
It makes use of the timeline. The timeline gets entries from a data provider, which generates the type and time of three lectures. Depending on the current time and the time of the lecture the entries are returned differently. The method `getCurrentTimelineEntry` returns the next upcoming lecture, `getTimelineEntries(...before...)` returns lectures which started before the given time and `getTimelineEntries(...after...)` returns lectures which will start after the given time. Those entries (lectures) are saved to the timeline to reduce power consumption by preloading them into the buffer. [Up until WatchOS 5 the user could also use a feature called time travel by using the digital crown](https://www.youtube.com/watch?v=om08gN-h0Bk), hence being able to look at past or future lectures. Technically this project would suffice the preconditions, but sadly this option is no longer available.

<img src="readme_images/complication_selection_3.png" width=25% height=25%>

## Architecture

Complications can be selected as a feature of a watch face in the Watch app on the iPhone or via the Apple Watch itself.

To present the user previews of the complications in the Watch App, image assets can be defined. These can be added in the ClockKit WatchKit Extension folder, as can be seen in this project.

To present the user localized previews of the complications, templates can be defined with the method `getLocalizableSampleTemplate` in the complication controller. This method is called once per supported complication and the results are cached. If a template is returned for a given complication, the defined asset is overwritten.

If neither of these previews are available, an actual timeline entry is generated with the method `getCurrentTimelineEntry` in the complication controller. Since this can be expensive, it is often not wanted.
